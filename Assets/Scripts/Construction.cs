﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Construction : MonoBehaviour {

	private Slider [] 	changeNodeTypeSL;
	public GameObject[] nodesType;
	public Transform[] nodeTransform;

	void Start () 
	{
		changeNodeTypeSL = new Slider[4];
		for (int i = 0; i < changeNodeTypeSL.Length; i++) {
			changeNodeTypeSL [i] = GameObject.Find("/Canvas/ChangeNodeTypeSL"+i).GetComponent<Slider>();
			changeNodeTypeSL [i].maxValue = 21;
		}
	}
	
	public void selectNodeSL1 ()
	{   
		ChangeNodeToSelected(0);
	}
	public void selectNodeSL2 ()
	{
		ChangeNodeToSelected (1);
	}
	public void selectNodeSL3 ()
	{
		ChangeNodeToSelected (2);
	}
	public void selectNodeSL4 ()
	{
		ChangeNodeToSelected (3);
	}


	void ChangeNodeToSelected(int nodeNumber)
	{   
		DeleteOldNode (nodeTransform [nodeNumber]);
		if (nodesType [(int)changeNodeTypeSL [nodeNumber].value] != null) 
		{
			GameObject nodeIns = Instantiate (nodesType [(int)changeNodeTypeSL [nodeNumber].value]) as GameObject;
			nodeIns.transform.parent = nodeTransform [nodeNumber].transform;
			nodeIns.transform.position = nodeTransform [nodeNumber].transform.position;
		};
	}

	void DeleteOldNode(Transform NodePositionTransform)
	{
		foreach (Transform oldTransform in NodePositionTransform) {
			GameObject.Destroy (oldTransform.gameObject);
		}

	}
}
